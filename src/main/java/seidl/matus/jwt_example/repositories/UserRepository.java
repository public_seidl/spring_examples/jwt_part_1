package seidl.matus.jwt_example.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import seidl.matus.jwt_example.model.domen.User;

import java.util.Optional;

/**
 * @author Matus Seidl (5+3)
 * 2018-01-10
 */
@Repository
public interface UserRepository extends JpaRepository<User,Long> {
    Optional<User> findByUsername(String username);
}
