package seidl.matus.jwt_example.controllers;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import seidl.matus.jwt_example.security.JwtGenerator;
import seidl.matus.jwt_example.security.JwtUser;

/**
 * @author Matus Seidl (5+3)
 * 2018-01-10
 */
@RestController
@RequestMapping("/token")
public class TokenController {

    private JwtGenerator jwtGenerator;

    public TokenController(JwtGenerator jwtGenerator) {
        this.jwtGenerator = jwtGenerator;
    }

    @PostMapping
    public String generate(@RequestBody final JwtUser jwtUser){
       
        return jwtGenerator.generate(jwtUser);
    }

}
