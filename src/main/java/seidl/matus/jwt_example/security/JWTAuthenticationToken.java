package seidl.matus.jwt_example.security;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

/**
 * @author Matus Seidl (5+3)
 * 2018-01-10
 */
public class JWTAuthenticationToken extends UsernamePasswordAuthenticationToken {

    private String token;

    public JWTAuthenticationToken( String token) {
        super(null, null);
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public Object getPrincipal() {
        return null;
    }
}
