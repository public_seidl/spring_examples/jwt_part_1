package seidl.matus.jwt_example.security;

import lombok.Data;

/**
 * @author Matus Seidl (5+3)
 * 2018-01-10
 */
@Data
public class JwtUser {
    private String userName;
    private Long id;
    private String role;

}
